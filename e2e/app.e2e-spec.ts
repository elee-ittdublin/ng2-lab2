import { NgLab2Page } from './app.po';

describe('ng-lab2 App', function() {
  let page: NgLab2Page;

  beforeEach(() => {
    page = new NgLab2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
